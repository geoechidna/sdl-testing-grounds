#include "Game.h"
#include <iostream>


int main(int argc, char* argv[]){
	Game* game = new Game();
	game->Init("test", 640, 480, false);
	game->ChangeState(s_intro);

	while (game->Running()){
		game->HandleEvents();
        game->Update();
        game->Draw();
    }

    // cleanup the engine
    game->Clean();

    return 0;
}

