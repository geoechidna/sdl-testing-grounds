#include "Game.h"

#include "States\list.h"
#include <ctime>
#include <limits>

using std::numeric_limits;

int SDLCALL EventIntercept(void * userdata, SDL_Event * event);

// constructor
Game::Game(){
}

void Game::Init(const char* title, int width, int height, bool fullscreen){
    
    int flags = 0;

    // initialize SDL
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0){
		cout << "Failed to initialise SDL: " << SDL_GetError() << endl;
		throw - 1;
	}

	if (fullscreen) {
		window = SDL_CreateWindow(title, 0, 0, 0, 0, SDL_WINDOW_FULLSCREEN_DESKTOP);
	}
	else {
		window = SDL_CreateWindow(title, 100, 100, width, height, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	}

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");  // make the scaled rendering look smoother.
	SDL_RenderSetLogicalSize(renderer, 640, 480);

    m_bFullscreen = fullscreen;

    m_bRunning = true;

	timeOpen = 0;

	SDL_AddEventWatch(EventIntercept, this);

	TTF_Init();
	font = TTF_OpenFont("\\Projects\\SDL Test\\Test 2\\exo2.ttf", 128);
	if (font == NULL){
		cout << TTF_GetError() << endl;
	}
	Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);
	

	stateFn[s_intro] =	(State*)IntroState::Create();
	stateFn[s_menu]	 =	(State*)MenuState::Create();
	stateFn[s_play]  =	(State*)PlayState::Create();
	stateFn[s_pause] =	(State*)PauseState::Create();
	//stateFn[s_opt] =  (State*)OptState::Create();
	Pushers = { s_pause, s_opt };



    // print our success
    printf("Game Initialised Succesfully\n");

}

void Game::ChangeState(stateList id){
	State* state = NULL;
	state = stateFn[id];
	if (state != NULL){
		if (std::find(std::begin(Pushers), std::end(Pushers), id) != std::end(Pushers))
			PushState(state);
		else
			SwapState(state);
	}
}

void Game::SwapState(State* state){
	if (!states.empty()){
		states.back()->Clean();
		states.pop_back();
	}
	states.push_back(state);
	states.back()->Init();
}

void Game::PushState(State* state){
	if (!states.empty()){
		states.back()->Pause();
	}

	states.push_back(state);
	states.back()->Init();
}

void Game::PopState(){
	if (!states.empty()){
		states.back()->Clean();
		states.pop_back();
		states.back()->Resume();
	}
}

void Game::HandleEvents(){
	SDL_PumpEvents();
	const Uint8* isPressed = SDL_GetKeyboardState(NULL);
	states.back()->HandleEvents(this, isPressed);
}

void Game::Update(){
	timeClose = timeOpen;
	timeOpen = clock();
	dt = (timeOpen - timeClose) / 1000.0;
	if (dt > 0.15)
		dt = 0.15;
	if (timeOpen > numeric_limits<clock_t>::max() - 1024){
		timeOpen -= 1024;
		timeClose -= 1024;
	}
	if (timeOpen/1000 != timeClose/1000)
		cout << timeOpen/1000 << endl;

	states.back()->Update(this, dt);
}

void Game::Draw(){
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderClear(renderer);
	states.back()->Draw(this);
	Render();
	SDL_RenderPresent(renderer);
}

void Game::Clean(){
	while (!states.empty()) {
		states.back()->Clean();
		states.pop_back();
	}
	// shutdown SDL
	SDL_Quit();
}

void Game::Render(){
}

int SDLCALL EventIntercept(void * userdata, SDL_Event * event){
	if (event->type == SDL_QUIT)
		((Game*)userdata)->Quit();
	/*	ANDROID AND iOS
	*if (event.type == SDL_APP_WILLENTERBACKGROUND)
	*if (event.type == SDL_APP_DIDENTERBACKGROUND)
	*if (event.type == SDL_APP_WILLENTERFOREGROUND)
	*if (event.type == SDL_APP_DIDENTERFOREGROUND)
	*if (event.type == SDL_APP_LOWMEMORY)
	*if (event.type == SDL_APP_TERMINATING)    */

	return 0;
}