#ifndef  _GAME_H_
#define _GAME_H_

#include <SDL.h>
#include <vector>
#include <unordered_map>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#include <iostream>

using std::cout;
using std::string;
using std::endl;

class State;

typedef enum stateList{
	s_init,
	s_menu,
	s_play,
	s_pause,
	s_intro,
	s_opt
}stateList;


class Game
{
public:
	Game();
	~Game();
    void Init(const char* title, int width, int height, bool fullscreen);
    
	void ChangeState(stateList id);
	void SwapState(State* state);
	void PushState(State* state);
	void PopState();

    void HandleEvents();
    void Update();
    void Draw();
    
    void Clean();

	bool Running() { return m_bRunning; }
	void Quit() { m_bRunning = false; }

	void Render();
	SDL_Renderer *renderer;
	TTF_Font* font;
	SDL_Window* window;


    
private:

	std::vector<State*> states;

	std::unordered_map<stateList, State*> stateFn;

	std::vector<int> Pushers;

    bool m_bRunning;
    bool m_bFullscreen;

	int timeOpen;
	int timeClose;
	double dt;

};

extern int SDLCALL EventIntercept(void * userdata, SDL_Event * event);

#endif