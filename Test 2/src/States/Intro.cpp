#include <stdio.h>

#include "Intro.h"
#include "../Game.h"
#include <math.h>


void IntroState::Init()
{
	timer = 0;
	alpha = 0;
	Mix_LoadMUS("../audio/startup.ogg");
	playedSound = false;
	textColour = { 255, 255, 255};
	printf("IntroState Init Successful\n");
}

void IntroState::Clean()
{

	printf("IntroState Clean Successful\n");
}

void IntroState::Pause()
{
	printf("IntroState Paused\n");
}

void IntroState::Resume()
{
	printf("IntroState Resumed\n");
}

void IntroState::HandleEvents(Game* game, const Uint8* isPressed){
}

void IntroState::Update(Game* game, double dt){
	timer += dt;
	if (timer > 1.5){
		game->ChangeState(s_menu);
	}
	else if (timer <= 1 && !playedSound){
		Mix_PlayChannel(-1, chikun, 0);
		playedSound = true;
	}
}

void IntroState::Draw(Game* game){
	if (timer < 0.5)
		alpha = (int)(2 * timer * 255);
	else if (timer < 1)
		alpha = 255;
	else
		alpha = (int)(2 * (1.5 - timer) * 255);
	textSurface = TTF_RenderText_Solid(game->font, "chikun", textColour);
	SDL_Texture* textTure = SDL_CreateTextureFromSurface(game->renderer, textSurface);
	SDL_SetTextureAlphaMod(textTure, alpha);
	int w = 0, h = 0;
	SDL_GetWindowSize(game->window, &w, &h);
	SDL_Rect *rect = new SDL_Rect();
	rect->x = w / 4;
	rect->y = 3 * h / 8;
	rect->w = w / 2;
	rect->h = h / 4;

	SDL_RenderCopy(game->renderer, textTure, NULL, rect);
}

