#ifndef  _PAUSE_STATE_H_
#define _PAUSE_STATE_H_

#include "State.h"

class PauseState : public State
{
public:
	PauseState(){}

	void Init();
	void Clean();

	void Pause();
	void Resume();

	void HandleEvents(Game* game, const Uint8* isPressed);
	void Update(Game* game, double dt);
	void Draw(Game* game);

	static State* Create(){ return new PauseState(); }


};


#endif

