#include <stdio.h>

#include "Pause.h"
#include "../Game.h"
#include "../Obj/Sprite.h"

void PauseState::Init()
{

	printf("PauseState Init Successful\n");
}

void PauseState::Clean()
{
	printf("PauseState Clean Successful\n");
}

void PauseState::Resume(){}

void PauseState::Pause() {}

void PauseState::HandleEvents(Game* game, const Uint8* isPressed)
{
	/*SDL_Event event;

	if (SDL_PollEvent(&event)) {
		switch (event.type) {
		case SDL_QUIT:
			game->Quit();
			break;

		case SDL_KEYDOWN:
			switch (event.key.keysym.sym){
			case SDLK_SPACE:
				game->PopState();
				break;
			}
		}
	}*/
}

void PauseState::Update(Game* game, double dt)
{
}

void PauseState::Draw(Game* game)
{
}

