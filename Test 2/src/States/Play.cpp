#include <stdio.h>
#include <iostream>

#include "Play.h"
#include "../Game.h"

PlayState::PlayState(){}

void PlayState::Init(){

	r = myRect();

	printf("PlayState Init Successful\n");
}

void PlayState::Clean(){
	printf("PlayState Clean Successful\n");
}

void PlayState::Pause(){
	printf("PlayState Paused\n");
}

void PlayState::Resume(){
	printf("PlayState Resumed\n");
}


void PlayState::HandleEvents(Game* game, const Uint8* isPressed){
	r.vx = r.vy = 0;
	if (isPressed[SDL_SCANCODE_UP]){
		r.vy--;
	}
	if (isPressed[SDL_SCANCODE_DOWN]){
		r.vy++;
	}
	if (isPressed[SDL_SCANCODE_LEFT]){
		r.vx--;
	}
	if (isPressed[SDL_SCANCODE_RIGHT]){
		r.vx++;
	}
}


void PlayState::Update(Game* game, double dt) {

	r.x += r.vx*r.s*dt;
	r.y += r.vy*r.s*dt;

}

void PlayState::Draw(Game* game)
{
	SDL_SetRenderDrawColor(game->renderer, 255, 255, 255, 255);
	SDL_RenderFillRect(game->renderer, &misc::newRect(r.x, r.y, r.w, r.h));
}

