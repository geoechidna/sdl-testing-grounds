#include <stdio.h>

#include "Menu.h"
#include "../Game.h"
#include "../misc.h"

void MenuState::Init()
{

	textInit = false;

	TitleColour = { 255, 255, 255, 255 };
	PlayColour = { 255, 0, 0, 255 };
	OptColour = { 0, 255, 0, 255 };
	ExitColour = { 0, 0, 255, 255 };

	CharWidth = 32;
	CharHeight = CharWidth * 2;
	CharWidthT = 64;
	CharHeightT = CharWidthT * 2;

	selected = 0;
	onSelect = true;
	selectBoxSpeed = 0;



	printf("MenuState Init Successful\n");
}

void MenuState::Clean()
{
	printf("MenuState Clean Successful\n");
}

void MenuState::Pause()
{
	printf("MenuState Paused\n");
}

void MenuState::Resume()
{
	printf("MenuState Resumed\n");
}

void MenuState::HandleEvents(Game* game, const Uint8* isPressed){
	
	if (onSelect){
		if (isPressed[SDL_SCANCODE_UP]){
			selected--;
			onSelect = false;
		}
		else if (isPressed[SDL_SCANCODE_DOWN]){
			selected++;
			onSelect = false;
		}
   		if (isPressed[SDL_SCANCODE_SPACE]){
			changeState(game, selected);
		}
	}

	if (selected < 0)
		selected = 2;
	if (selected > 2)
		selected = 0;
	

	if (Title == NULL || PlayOpt == NULL || OptionsOpt == NULL || ExitOpt == NULL){
		SDL_Surface* sTitle;
		SDL_Surface* sPlayOpt;
		SDL_Surface* sOptionsOpt;
		SDL_Surface* sExitOpt;
		sTitle = TTF_RenderText_Blended(game->font, "Game Test", TitleColour);
		TitleLen = 9;
		sPlayOpt = TTF_RenderText_Blended(game->font, "Play Game", PlayColour);
		PlayLen = 9;
		sOptionsOpt = TTF_RenderText_Blended(game->font, "Options", OptColour);
		OptionsLen = 7;
		sExitOpt = TTF_RenderText_Blended(game->font, "Exit", ExitColour);
		ExitLen = 4;
		Title = SDL_CreateTextureFromSurface(game->renderer, sTitle);
		PlayOpt = SDL_CreateTextureFromSurface(game->renderer, sPlayOpt);
		OptionsOpt = SDL_CreateTextureFromSurface(game->renderer, sOptionsOpt);
		ExitOpt = SDL_CreateTextureFromSurface(game->renderer, sExitOpt);
		SDL_FreeSurface(sTitle);
		SDL_FreeSurface(sPlayOpt);
		SDL_FreeSurface(sOptionsOpt);
		SDL_FreeSurface(sExitOpt);
	}
}

void MenuState::Update(Game* game, double dt){

	int x = 0, y = 0;
	SDL_GetWindowSize(game->window, &x, &y);

	selectBoxX = x / 2 - CharWidth * 5;
	selectBoxY = (selected + 3) * y / 6 - CharHeight / 2 - 8;
	selectBoxW = CharWidth * 10;
	selectBoxH = CharHeight + 16;

	if (!onSelect){
		selectBoxCurrentV = selectBoxCurrentY < selectBoxY ? 1 : -1;
		if (abs(selectBoxSpeed) < 0.1) {
			selectBoxSpeed = abs(selectBoxY - selectBoxCurrentY) * 4;
		}
		if (abs(selectBoxCurrentY - selectBoxY) < selectBoxSpeed * dt){
			selectBoxCurrentV = 0;
			onSelect = true;
		}
	}

	if (onSelect){
		selectBoxCurrentV = 0;
		selectBoxCurrentY = selectBoxY;
		selectBoxSpeed = 0;
	}
	else{
		selectBoxCurrentY += selectBoxCurrentV * selectBoxSpeed * dt;
	}
}

void MenuState::Draw(Game* game){

	int x = 0, y = 0;
	SDL_GetWindowSize(game->window, &x, &y);
	int w = 0, h = 0;

	SDL_RenderCopy(game->renderer, Title, NULL, &(misc::newRect(x / 2 - CharWidthT * TitleLen / 2, y / 6 - CharHeightT / 2, CharWidthT * TitleLen, CharHeightT)));
	SDL_RenderCopy(game->renderer, PlayOpt, NULL, &(misc::newRect(x / 2 - CharWidth * PlayLen / 2, 3 * y / 6 - CharHeight / 2, CharWidth * PlayLen, CharHeight)));
	SDL_RenderCopy(game->renderer, OptionsOpt, NULL, &(misc::newRect(x / 2 - CharWidth * OptionsLen / 2, 4 * y / 6 - CharHeight / 2, CharWidth * OptionsLen, CharHeight)));
	SDL_RenderCopy(game->renderer, ExitOpt, NULL, &(misc::newRect(x / 2 - CharWidth * ExitLen / 2, 5 * y / 6 - CharHeight / 2, CharWidth * ExitLen, CharHeight)));

	SDL_Rect* selRect = new SDL_Rect();
	selRect->x = (int)selectBoxX;
	selRect->y = (int)selectBoxCurrentY;
	selRect->h = (int)selectBoxH;
	selRect->w = (int)selectBoxW;
	SDL_SetRenderDrawColor(game->renderer, 255, 255, 255, 255);
	SDL_RenderDrawRect(game->renderer, selRect);


}

void MenuState::changeState(Game* game, int to){
	switch (to){
	case 0: game->ChangeState(s_play);
		break;
	case 1: game->ChangeState(s_opt);
		break;
	case 2: game->Quit();
		break;
	}
}