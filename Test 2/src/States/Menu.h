#ifndef _MENU_STATE_H_
#define _MENU_STATE_H_

#include "State.h"

class MenuState : public State{
public:
	MenuState(){}

	void Init();
	void Clean();

	void Pause();
	void Resume();

	void HandleEvents(Game* game, const Uint8* isPressed);
	void Update(Game* game, double dt);
	void Draw(Game* game);
	
	static State* Create(){ return new MenuState(); }

private:

	bool textInit;
	SDL_Colour TitleColour;
	SDL_Colour PlayColour;
	SDL_Colour OptColour;
	SDL_Colour ExitColour;
	SDL_Texture* Title;
	SDL_Texture* PlayOpt;
	SDL_Texture* OptionsOpt;
	SDL_Texture* ExitOpt;
	int TitleLen;
	int PlayLen;
	int OptionsLen;
	int ExitLen;
	int CharWidth;
	int CharHeight;
	int CharWidthT;
	int CharHeightT;

	int selected;
	bool onSelect;

	int selectBoxX;
	int selectBoxY;
	int selectBoxW;
	int selectBoxH;
	double selectBoxCurrentY;
	double selectBoxCurrentV;
	double selectBoxSpeed;

	void changeState(Game*, int);

};

#endif

