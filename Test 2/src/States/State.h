#ifndef STATE_H
#define STATE_H

#include <SDL.h>

class Game;

class State{
public:
	virtual void Init() = 0;
	virtual void Clean() = 0;

	virtual void Pause() = 0;
	virtual void Resume() = 0;

	virtual void HandleEvents(Game* game, const Uint8* isPressed) = 0;
	virtual void Update(Game* game, double dt) = 0;
	virtual void Draw(Game* game) = 0;

	// static virtual State* Create();
};
#endif

