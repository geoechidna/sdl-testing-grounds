#ifndef  _PLAY_STATE_H_
#define _PLAY_STATE_H_


#include "../misc.h"
#include "State.h"
#include "../Obj/GameObject.h"

class PlayState : public State
{
public:

	PlayState();

	void Init();
	void Clean();

	void Pause();
	void Resume();

	void HandleEvents(Game* game, const Uint8* isPressed);
	void Update(Game* game, double dt);
	void Draw(Game* game);
	
	static State* Create(){ return new PlayState(); }

private:
	myRect r;


};

#endif

