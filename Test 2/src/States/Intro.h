#ifndef _INTRO_STATE_H_
#define _INTRO_STATE_H_

#include "State.h"
#include "../Obj/Sprite.h"

class IntroState : public State{
public:
	IntroState(){}

	void Init();
	void Clean();

	void Pause();
	void Resume();

	void HandleEvents(Game* game, const Uint8* isPressed);
	void Update(Game* game, double dt);
	void Draw(Game* game);
	
	static State* Create(){ return new IntroState(); }

private:
	double timer;
	SDL_Colour textColour;
	SDL_Surface* textSurface;
	int alpha;
	bool playedSound;
	struct Mix_Chunk *chikun;

};

#endif

