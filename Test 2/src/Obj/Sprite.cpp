#include "Sprite.h"

// constructor 
Sprite::Sprite()
{
}


SDL_Texture* Sprite::Load(SDL_Renderer *renderer, char* File)
{
	SDL_Surface* loadSurface = NULL;

	if ((loadSurface = SDL_LoadBMP(File)) == NULL)
	{
		return NULL;
	}

	SDL_Texture* transform = SDL_CreateTextureFromSurface(renderer, loadSurface);

	return transform;
}


bool Sprite::Draw(SDL_Renderer* dest, SDL_Texture* src, int posx, int posy)
{
	if (dest == NULL || src == NULL)
	{
		return false;
	}

	SDL_Rect  destR;

	destR.x = posx;
	destR.y = posy;

	//SDL_BlitSurface(src, NULL, dest, &destR);

	return true;
}


bool Sprite::Draw(SDL_Renderer* dest, SDL_Texture* src, int posx, int posy, int snipx, int snipy, int snipw, int sniph) {
	if (dest == NULL || src == NULL) {
		return false;
	}

	SDL_Rect destR;

	destR.x = posx;
	destR.y = posx;

	SDL_Rect srcR;

	srcR.x = snipx;
	srcR.y = snipy;
	srcR.w = snipw;
	srcR.h = sniph;

	//SDL_BlitSurface(src, &srcR, dest, &destR);

	return true;
}





