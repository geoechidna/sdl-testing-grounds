#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include "../Game.h"

class GameObject{
public:

	void LoadSprite(char* filename);

	void HandleEvents(Game* game, const Uint8* isPressed);
	void Update(Game* game, double dt);
	void Draw(Game* game);

	void Clean();

private:

	 SDL_Texture* sprite;

};

#endif

