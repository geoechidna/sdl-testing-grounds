#ifndef  _SPRITE_H_
#define _SPRITE_H_

#include <SDL.h>
#include <SDL_image.h>

class Sprite
{
public:

	Sprite();

	static int getWidth(SDL_Texture* tex);
	static int getHeight(SDL_Texture* tex);
	static SDL_Texture* Load(SDL_Renderer* renderer, char* pFile);
	static bool Draw(SDL_Renderer* dest, SDL_Texture* src, int posx, int posy);
	static bool Draw(SDL_Renderer* dest, SDL_Texture* src, int posx, int posy, int snipx, int snipy, int snipw, int sniph);
};


#endif

