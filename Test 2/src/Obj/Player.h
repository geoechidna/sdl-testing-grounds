#ifndef  _PLAYER
#define _PLAYER

#include "GameObject.h"
#include "Sprite.h"


class Player : public GameObject{
public:
	
	void Load(char* filename);
	void HandleEvents();
	void Update();
	void Draw();
	void Clean();

private:

	SDL_Texture* sprite;

};

#endif